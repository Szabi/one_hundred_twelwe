﻿using System;
using System.Text;

namespace _112
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Please type a number and then press enter");
			string limit = Console.ReadLine();
			String numberChain = GetHighestNumber(GenerateNumber(int.Parse(limit)));
			Console.WriteLine(numberChain);
			Console.WriteLine("Press any key to escape");
			Console.ReadKey();
		}

		static String GetHighestNumber(String numberChain)
		{
			int numberChainOriginalLength = numberChain.Length;
			int i = 0;
			int j = i + 1;
			StringBuilder removed = new StringBuilder();

			while (numberChain.Length > numberChainOriginalLength / 2)
			{
				if(numberChain[i] == '9')
				{
					i++;
				} else
				{
					j = i + 1;
					if(char.GetNumericValue(numberChain[i]) == char.GetNumericValue(numberChain[j]))
					{
						i++;
						continue;
					}
					if (char.GetNumericValue(numberChain[i]) < char.GetNumericValue(numberChain[j]))
					{
						removed.Append(numberChain[i]);
						numberChain = numberChain.Remove(i, 1);
						while (i > 0 && numberChain[--i] != '9' && numberChain.Length > numberChainOriginalLength / 2)
						{
							removed.Append(numberChain[i]);
							numberChain = numberChain.Remove(i, 1);
						}
					} else
					{
						int k = j + 1;
						while(char.GetNumericValue(numberChain[j]) > char.GetNumericValue(numberChain[k]))
						{
							k++;
							j++;
						}
						removed.Append(numberChain[j]);
						numberChain = numberChain.Remove(j, 1);
					}
				}
			}
			//return removed.ToString();
			return numberChain;
		}

		static String GenerateNumber(int limit)
		{
			StringBuilder numberChain = new StringBuilder();
			for(int i = 1; i <= limit; i++)
			{
				numberChain.Append(i);
			}
			return numberChain.ToString();
		}
	}
}
