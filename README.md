# 112

## Requirements

.NET 3.1 ( https://dotnet.microsoft.com/download/dotnet/3.1 )

## Instructions

Build the project by running `dotnet build 112` or run it with `dotnet run 112` in the 112 folder


## Description

This was made for the 112th "ész ventura" puzzle on Qubit ( https://qubit.hu/2021/03/08/esz-ventura-vegre-egy-konnyu-feladat )
The console application requires you to type a number and then it will create a number chain from 1 to the typed number and then it will remove half the digits by the logic that the result number should be the highest possible

## Improvement opportunities
Input validity check can be added
